package com.upcmoviles.appmobile;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.upcmoviles.appmobile.model.HorarioProfe;
import com.upcmoviles.appmobile.model.ProfeCurso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class HorariosProfe extends AppCompatActivity {

    RecyclerView recyclerView;
    ArrayList<HorarioProfe> listaHorarios = new ArrayList<>();
    HorarioProfeAdapter adapter;
    //ListView lstProfecurso;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_horarios_profe);


        int idprofe= getIntent().getExtras().getInt("idprofe");
        int idcurso= getIntent().getExtras().getInt("idcurso");
        String nombreprofe= getIntent().getExtras().getString("nombreprofe");
        String nombrecurso= getIntent().getExtras().getString("nombrecurso");
        String nroceular=getIntent().getExtras().getString("nrocelular");

        TextView txt_profe_nombres= (TextView)findViewById(R.id.txt_profe_nombres);
        txt_profe_nombres.setText(nombreprofe);
        TextView txt_curso= (TextView)findViewById(R.id.txt_curso);
        txt_curso.setText(nombrecurso);
        EditText edtcelular= (EditText)findViewById(R.id.edt_fono);
        edtcelular.setText(nroceular);
        System.out.println("  horarios - idprofe :" + idprofe);
        getHorariosbyProfe(idprofe);
    }

    private void getHorariosbyProfe(int idprofe)
    {
        String url = "http://atasilla.atwebpages.com/index.php/horarios/" + idprofe ;

        System.out.println("Intentando conectar al servicio horarios - idprofe :" + idprofe);
        StringRequest peticion = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    JSONArray arreglo = new JSONArray(response);
                    List<String> items = new ArrayList<>();
                    HorarioProfe horarioprofe= new HorarioProfe();
                    System.out.println("Datos econtrados " + arreglo.length());
                    for(int i=0;i<arreglo.length();i++){
                        JSONObject object = arreglo.getJSONObject(i);
                        horarioprofe=new HorarioProfe();
                        horarioprofe.setDiatexto(object.getString("diatexto"));
                        horarioprofe.setNrodiasemana(object.getInt("nrodiasemana"));
                        horarioprofe.setIdhorario(object.getInt("idhorario"));
                        horarioprofe.setHoraIni(object.getInt("horaini"));
                        horarioprofe.setHoraFin(object.getInt("horafin"));

                        //   items.add(object.getString("nombre") + " (S/." + object.getString("precio") +")");
                        listaHorarios.add(horarioprofe);
                        // ArrayAdapter<Curso> adapter = new ArrayAdapter<Curso>(ListaCursos.this, android.R.layout.simple_list_item_1,items);
                        // lstProductos.setAdapter(adapter);
                       // System.out.println("nombre curso:" + object.getString("nombres"));

                    }

                    recyclerView = findViewById(R.id.recyclerView);
                    System.out.println("Leyendo servicio");
                    adapter = new HorarioProfeAdapter(HorariosProfe.this,listaHorarios);
                    recyclerView.setAdapter(adapter);
                    System.out.println( "nro filas: " + listaHorarios.size() );
                    recyclerView.setLayoutManager(new LinearLayoutManager(HorariosProfe.this));
                    System.out.println("Fin servicio");

                }catch (JSONException e){
                    Toast.makeText(HorariosProfe.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("error" + error.getMessage());
                Toast.makeText(HorariosProfe.this, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        RequestQueue cola = Volley.newRequestQueue(this);
        cola.add(peticion);

        recyclerView = findViewById(R.id.recyclerView);
        System.out.println("Leyendo servicio");
        adapter = new HorarioProfeAdapter(HorariosProfe.this,listaHorarios);
        recyclerView.setAdapter(adapter);
        System.out.println( "nro filas: " + listaHorarios.size() );
        recyclerView.setLayoutManager(new LinearLayoutManager(HorariosProfe.this));
        System.out.println("Fin servicio");
        //return  listaCursos;
    }
}