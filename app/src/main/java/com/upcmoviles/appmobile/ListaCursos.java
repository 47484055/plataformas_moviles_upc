package com.upcmoviles.appmobile;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.upcmoviles.appmobile.R;
import com.upcmoviles.appmobile.model.Curso;

import java.util.ArrayList;
import java.util.List;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class ListaCursos extends AppCompatActivity {

    RecyclerView recyclerView;
    ArrayList<Curso> listaCursos = new ArrayList<>();
    CustomAdapter customAdapter;
   // ListView lstProductos;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_cursos);

        mostrarListaCursos();
    }

    private void mostrarListaCursos(){

      //  listaCursos=getAllCursos();
        getAllCursos();
        recyclerView = findViewById(R.id.recyclerView);
        System.out.println("Leyendo servicio");
            customAdapter = new CustomAdapter(ListaCursos.this,listaCursos);
        recyclerView.setAdapter(customAdapter);
        System.out.println( "nro filas: " + listaCursos.size() );
        recyclerView.setLayoutManager(new LinearLayoutManager(ListaCursos.this));
        System.out.println("Fin servicio");

    }

    private void getAllCursos()
    {
        String url = "http://atasilla.atwebpages.com/index.php/cursos";

        System.out.println("Intentando conectar al servicio");
        StringRequest peticion = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    JSONArray arreglo = new JSONArray(response);
                    List<String> items = new ArrayList<>();
                    Curso curso = new Curso();
                    System.out.println("Datos econtrados " + arreglo.length());
                    for(int i=0;i<arreglo.length();i++){
                        JSONObject object = arreglo.getJSONObject(i);
                        curso=new Curso();
                     curso.setNombrecurso(object.getString("nombrecurso"));
                     curso.setDescripcion(object.getString("descripcion"));
                        curso.setFileimagen(object.getString("fileimagen"));
                        curso.setIdcurso(object.getInt("idcurso"));
                        //   items.add(object.getString("nombre") + " (S/." + object.getString("precio") +")");
                        listaCursos.add(curso);
                       // ArrayAdapter<Curso> adapter = new ArrayAdapter<Curso>(ListaCursos.this, android.R.layout.simple_list_item_1,items);
                      // lstProductos.setAdapter(adapter);
                        System.out.println("nombre curso:" + object.getString("nombrecurso"));

                    }

                    recyclerView = findViewById(R.id.recyclerView);
                    System.out.println("Leyendo servicio");
                    customAdapter = new CustomAdapter(ListaCursos.this,listaCursos);
                    recyclerView.setAdapter(customAdapter);
                    System.out.println( "nro filas: " + listaCursos.size() );
                    recyclerView.setLayoutManager(new LinearLayoutManager(ListaCursos.this));
                    System.out.println("Fin servicio");

                }catch (JSONException e){
                    Toast.makeText(ListaCursos.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("error" + error.getMessage());
                Toast.makeText(ListaCursos.this, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        RequestQueue cola = Volley.newRequestQueue(this);
        cola.add(peticion);

        recyclerView = findViewById(R.id.recyclerView);
        System.out.println("Leyendo servicio");
        customAdapter = new CustomAdapter(ListaCursos.this,listaCursos);
        recyclerView.setAdapter(customAdapter);
        System.out.println( "nro filas: " + listaCursos.size() );
        recyclerView.setLayoutManager(new LinearLayoutManager(ListaCursos.this));
        System.out.println("Fin servicio");
      //return  listaCursos;
    }
}