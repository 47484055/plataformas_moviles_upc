package com.upcmoviles.appmobile.model;

public class HorarioProfe {

private int  idprofehorario ;
private int idprofesor;
private int idhorario;
private String diatexto;
private int nrodiasemana;
private int horaIni;
private int horaFin;

    public int getIdprofehorario() {
        return idprofehorario;
    }

    public void setIdprofehorario(int idprofehorario) {
        this.idprofehorario = idprofehorario;
    }

    public int getIdprofesor() {
        return idprofesor;
    }

    public void setIdprofesor(int idprofesor) {
        this.idprofesor = idprofesor;
    }

    public int getIdhorario() {
        return idhorario;
    }

    public void setIdhorario(int idhorario) {
        this.idhorario = idhorario;
    }

    public String getDiatexto() {
        return diatexto;
    }

    public void setDiatexto(String diatexto) {
        this.diatexto = diatexto;
    }

    public int getNrodiasemana() {
        return nrodiasemana;
    }

    public void setNrodiasemana(int nrodiasemana) {
        this.nrodiasemana = nrodiasemana;
    }

    public int getHoraIni() {
        return horaIni;
    }

    public void setHoraIni(int horaIni) {
        this.horaIni = horaIni;
    }

    public int getHoraFin() {
        return horaFin;
    }

    public void setHoraFin(int horaFin) {
        this.horaFin = horaFin;
    }
}
