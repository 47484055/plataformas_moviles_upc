package com.upcmoviles.appmobile.model;

public class ProfeCurso {

    private int idprofe;
    private String nombres;
   private String apellidos;
    private String correo;
    private String direccion;
    private String fileimagen;
    private String genero;
 private double   tarifahora;
 private String nrocelular;
    private int idcurso;
    private  String nombrecurso;

public  ProfeCurso(){}

    public int getIdprofe() {
        return idprofe;
    }

    public void setIdprofe(int idprofe) {
        this.idprofe = idprofe;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getFileimagen() {
        return fileimagen;
    }

    public void setFileimagen(String fileimagen) {
        this.fileimagen = fileimagen;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public int getIdcurso() {
        return idcurso;
    }

    public void setIdcurso(int idcurso) {
        this.idcurso = idcurso;
    }

    public double getTarifahora() {
        return tarifahora;
    }

    public void setTarifahora(double tarifahora) {
        this.tarifahora = tarifahora;
    }

    public String getNrocelular() {
        return nrocelular;
    }

    public void setNrocelular(String nrocelular) {
        this.nrocelular = nrocelular;
    }

    public String getNombrecurso() {
        return nombrecurso;
    }

    public void setNombrecurso(String nombrecurso) {
        this.nombrecurso = nombrecurso;
    }
}
