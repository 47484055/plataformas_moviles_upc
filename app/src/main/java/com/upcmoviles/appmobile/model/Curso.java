package com.upcmoviles.appmobile.model;

public class Curso {

    private int id;
    private int idcurso;
    private String nombrecurso;
    private String descripcion;
    private String abreviatura;
    private String fileimagen;

public  Curso(){}
    public Curso(String nombrecurso,String descripcion,String abreviatura, String fileimagen)
    {
        this.nombrecurso=nombrecurso;
        this.setDescripcion(descripcion);
        this.setAbreviatura(abreviatura);
        this.setFileimagen(fileimagen);

    }

    public Curso(int idcurso, String nombrecurso,String descripcion,String abreviatura, String fileimagen)
    {
        this.idcurso=idcurso;
        this.nombrecurso=nombrecurso;
        this.setDescripcion(descripcion);
        this.setAbreviatura(abreviatura);
        this.setFileimagen(fileimagen);

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdcurso() {
        return idcurso;
    }

    public void setIdcurso(int idcurso) {
        this.idcurso = idcurso;
    }

    public String getNombrecurso() {
        return nombrecurso;
    }

    public void setNombrecurso(String nombrecurso) {
        this.nombrecurso = nombrecurso;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getAbreviatura() {
        return abreviatura;
    }

    public void setAbreviatura(String abreviatura) {
        this.abreviatura = abreviatura;
    }

    public String getFileimagen() {
        return fileimagen;
    }

    public void setFileimagen(String fileimagen) {
        this.fileimagen = fileimagen;
    }
}
