package com.upcmoviles.appmobile;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.upcmoviles.appmobile.R;

public class MainActivity extends AppCompatActivity {


    ImageView imgAlumno;
    ImageView imgProfe;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        gotoAlumnoLayout();
        gotoProfeLayout();

    }

private void gotoAlumnoLayout(){
    imgAlumno = findViewById(R.id.imgAlumno);

    imgAlumno.setOnClickListener(new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(MainActivity.this,ListaCursos.class);
            startActivity(intent);
        }
    });
}
    private void gotoProfeLayout(){
        imgProfe = findViewById(R.id.imgProfe);

        imgProfe.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,ProfePerfil.class);
                startActivity(intent);
            }
        });
    }


}