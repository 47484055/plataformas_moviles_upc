package com.upcmoviles.appmobile;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.upcmoviles.appmobile.model.Curso;

import java.util.ArrayList;

public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<Curso> listCursos = new ArrayList<>();

    public CustomAdapter(Context context, ArrayList<Curso> listCursos){
        this.context = context;
        this.listCursos = listCursos;
    }

    @NonNull
    @Override
    public CustomAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.listacursos,parent,false);
        return  new MyViewHolder(view);

        //return null;
    }

    @Override
    public void onBindViewHolder(@NonNull  CustomAdapter.MyViewHolder holder, int position) {
        System.out.println("viewholder:" + listCursos.get(position).getNombrecurso() );
        holder.curso_nombre_txt.setText(String.valueOf(listCursos.get(position).getNombrecurso()));
        holder.curso_descripcion_txt.setText(String.valueOf(listCursos.get(position).getDescripcion()));
        System.out.println("posicion " + position);

        Resources res=context.getResources();
        //TypedValue obj=new TypedValue();
        //res.getValue("",  obj, true);

        System.out.println("imgen " + String.valueOf(listCursos.get(position).getFileimagen().replace(".png","")));

        System.out.println(" resource path "+ context.getPackageResourcePath());
        System.out.println(" resource package "+ context.getPackageName());

    //    System.out.println("res " + res.getIdentifier(String.valueOf(listCursos.get(position).getFileimagen().replace(".png","")),"drawable", context.getPackageName()));
      //  System.out.println("res " + res.getDrawable( res.getIdentifier(String.valueOf(listCursos.get(position).getFileimagen().replace(".png","")),"drawable", context.getPackageName())));
  holder.imgview.setImageDrawable(res.getDrawable(res.getIdentifier(String.valueOf(listCursos.get(position).getFileimagen().replace(".png","")),"drawable", context.getPackageName())));

        holder.itemView.setTag(position);

        holder.imgview.setOnClickListener((v)->{
            Intent intent=new Intent(context, ListaProfeCurso.class);
            intent.putExtra("nombre_curso" , listCursos.get(position).getNombrecurso());
            intent.putExtra("id_curso" , listCursos.get(position).getIdcurso());
            context.startActivity(intent);
        });
  /*holder.imgview.setClickable(new View.OnClickListener() {

      @Override
      public void onClick(View view) {
          Intent intent = new Intent(MainActivity.this,ProfePerfil.class);
          startActivity(intent);
      }
  });*/
     /*   holder.cita_paciente_txt.setText(String.valueOf(listaCitasMedicas.get(position).getPaciente()));
        holder.cita_especialidad_txt.setText(String.valueOf(listaCitasMedicas.get(position).getEspecialidad()));
        holder.cita_clinica_txt.setText(String.valueOf(listaCitasMedicas.get(position).getClinica()));
 */
    }

    @Override
    public int getItemCount() {
        return listCursos.size();
        //return 0;
    }

    public  class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView curso_nombre_txt,curso_descripcion_txt ;
        ImageView imgview ;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            curso_nombre_txt=itemView.findViewById(R.id.curso_nombre_txt);
            curso_descripcion_txt=itemView.findViewById(R.id.curso_descripcion_txt);
            imgview= itemView.findViewById(R.id.imageView);

     /*       cita_doctor_txt = itemView.findViewById(R.id.cita_doctor_txt);
            cita_paciente_txt = itemView.findViewById(R.id.cita_paciente_txt);
            cita_especialidad_txt = itemView.findViewById(R.id.cita_especialidad_txt);
            cita_clinica_txt = itemView.findViewById(R.id.cita_clinica_txt);
            cita_costo_txt = itemView.findViewById(R.id.cita_costo_txt);

            */

        }

        @Override
        public void onClick(View v) {
            final int position = (int) itemView.getTag();
            Toast.makeText(itemView.getContext(), Integer.toString(position), Toast.LENGTH_SHORT).show();
        }
    }
}
