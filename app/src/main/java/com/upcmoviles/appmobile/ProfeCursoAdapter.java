package com.upcmoviles.appmobile;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.upcmoviles.appmobile.model.Curso;
import com.upcmoviles.appmobile.model.ProfeCurso;

import java.util.ArrayList;

public class ProfeCursoAdapter extends RecyclerView.Adapter<ProfeCursoAdapter.MyViewHolder>{


    private Context context;
    private ArrayList<ProfeCurso> listProfeCurso = new ArrayList<>();

    public ProfeCursoAdapter(Context context, ArrayList<ProfeCurso> listProfeCurso){
        this.context = context;
        this.listProfeCurso = listProfeCurso;
    }

    @NonNull
    @Override
    public ProfeCursoAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.listaprofecurso,parent,false);
        return  new ProfeCursoAdapter.MyViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull  ProfeCursoAdapter.MyViewHolder holder, int position) {
        System.out.println("viewholder:" + listProfeCurso.get(position).getNombres() );
        holder.profe_nombre_txt.setText(String.valueOf(listProfeCurso.get(position).getNombres()));
        holder.profe_apellido_txt.setText(String.valueOf(listProfeCurso.get(position).getApellidos()));
        holder.profe_tarifa_txt.setText( "S/." + String.valueOf(listProfeCurso.get(position).getTarifahora()) + "/hra.");
        System.out.println("posicion " + position);

        Resources res=context.getResources();
        //TypedValue obj=new TypedValue();
        //res.getValue("",  obj, true);

        System.out.println("imgen " + String.valueOf(listProfeCurso.get(position).getFileimagen().replace(".png","")));

        System.out.println(" resource path "+ context.getPackageResourcePath());
        System.out.println(" resource package "+ context.getPackageName());

        //    System.out.println("res " + res.getIdentifier(String.valueOf(listCursos.get(position).getFileimagen().replace(".png","")),"drawable", context.getPackageName()));
        //  System.out.println("res " + res.getDrawable( res.getIdentifier(String.valueOf(listCursos.get(position).getFileimagen().replace(".png","")),"drawable", context.getPackageName())));
        holder.imgview.setImageDrawable(res.getDrawable(res.getIdentifier(String.valueOf(listProfeCurso.get(position).getFileimagen().replace(".png","")),"drawable", context.getPackageName())));

        holder.itemView.setTag(position);

        holder.imgview.setOnClickListener((v)->{
            Intent intent=new Intent(context, HorariosProfe.class);
            intent.putExtra("nombreprofe" , listProfeCurso.get(position).getNombres() + " " + listProfeCurso.get(position).getApellidos());
            intent.putExtra("idprofe" , listProfeCurso.get(position).getIdprofe());
            intent.putExtra("idcurso" , listProfeCurso.get(position).getIdcurso());
            intent.putExtra("nombrecurso" , listProfeCurso.get(position).getNombrecurso());
            intent.putExtra("nrocelular" , listProfeCurso.get(position).getNrocelular());
            context.startActivity(intent);
        });

        System.out.println( "id profe xx: "+ listProfeCurso.get(position).getIdprofe());

  /*holder.imgview.setClickable(new View.OnClickListener() {

      @Override
      public void onClick(View view) {
          Intent intent = new Intent(MainActivity.this,ProfePerfil.class);
          startActivity(intent);
      }
  });*/
     /*   holder.cita_paciente_txt.setText(String.valueOf(listaCitasMedicas.get(position).getPaciente()));
        holder.cita_especialidad_txt.setText(String.valueOf(listaCitasMedicas.get(position).getEspecialidad()));
        holder.cita_clinica_txt.setText(String.valueOf(listaCitasMedicas.get(position).getClinica()));
 */
    }

    @Override
    public int getItemCount() {
        return listProfeCurso.size();
        //return 0;
    }

    public  class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView profe_nombre_txt, profe_apellido_txt, profe_tarifa_txt ;
        ImageView imgview ;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            profe_nombre_txt = itemView.findViewById(R.id.profe_nombre_txt);
            profe_apellido_txt = itemView.findViewById(R.id.profe_apellido_txt);
            profe_tarifa_txt=itemView.findViewById(R.id.profe_tarifa_txt);
            imgview= itemView.findViewById(R.id.profeImageView);

     /*       cita_doctor_txt = itemView.findViewById(R.id.cita_doctor_txt);
            cita_paciente_txt = itemView.findViewById(R.id.cita_paciente_txt);
            cita_especialidad_txt = itemView.findViewById(R.id.cita_especialidad_txt);
            cita_clinica_txt = itemView.findViewById(R.id.cita_clinica_txt);
            cita_costo_txt = itemView.findViewById(R.id.cita_costo_txt);

            */

        }

        @Override
        public void onClick(View v) {
            final int position = (int) itemView.getTag();
            Toast.makeText(itemView.getContext(), Integer.toString(position), Toast.LENGTH_SHORT).show();
        }
    }
}
