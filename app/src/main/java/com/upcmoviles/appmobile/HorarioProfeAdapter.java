package com.upcmoviles.appmobile;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.upcmoviles.appmobile.model.HorarioProfe;
import com.upcmoviles.appmobile.model.ProfeCurso;

import java.util.ArrayList;

public class HorarioProfeAdapter extends RecyclerView.Adapter<HorarioProfeAdapter.MyViewHolder>{

    private Context context;
    private ArrayList<HorarioProfe> listHorarios = new ArrayList<>();

    public HorarioProfeAdapter(Context context, ArrayList<HorarioProfe> listHorarios){
        this.context = context;
        this.listHorarios = listHorarios;
    }


    @NonNull
    @Override
    public HorarioProfeAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.listahorariosprofe,parent,false);
        return  new HorarioProfeAdapter.MyViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull  HorarioProfeAdapter.MyViewHolder holder, int position) {
        System.out.println("viewholder:" + listHorarios.get(position).getDiatexto() );
        holder.horario_diatexto_txxt.setText(String.valueOf(listHorarios.get(position).getDiatexto()));
        holder.horario_dianro_txt.setText(String.valueOf(listHorarios.get(position).getNrodiasemana()));
        holder.horario_hora_txt.setText( String.valueOf(listHorarios.get(position).getHoraIni()) + " - "+ String.valueOf(listHorarios.get(position).getHoraFin()));
        System.out.println("posicion " + position);

        Resources res=context.getResources();
        //TypedValue obj=new TypedValue();
        //res.getValue("",  obj, true);

        //System.out.println("imgen " + String.valueOf(listHorarios.get(position).getFileimagen().replace(".png","")));

        System.out.println(" resource path "+ context.getPackageResourcePath());
        System.out.println(" resource package "+ context.getPackageName());

        //    System.out.println("res " + res.getIdentifier(String.valueOf(listCursos.get(position).getFileimagen().replace(".png","")),"drawable", context.getPackageName()));
        //  System.out.println("res " + res.getDrawable( res.getIdentifier(String.valueOf(listCursos.get(position).getFileimagen().replace(".png","")),"drawable", context.getPackageName())));
        //holder.imgview.setImageDrawable(res.getDrawable(res.getIdentifier(String.valueOf(listProfeCurso.get(position).getFileimagen().replace(".png","")),"drawable", context.getPackageName())));

        holder.itemView.setTag(position);

      holder.cdview.setOnClickListener((v)->{
        //
          AlertDialog.Builder alert = new AlertDialog.Builder(context);
          alert.setTitle("Reservar");
          alert.setMessage("Estas seguro de reservar");
          alert.setPositiveButton("Si", new DialogInterface.OnClickListener() {
              @Override
              public void onClick(DialogInterface dialog, int which) {
                 // Toast.makeText(context, "Item 1" + String.valueOf(listHorarios.get(position).getDiatexto()), Toast.LENGTH_SHORT).show();
                  Toast.makeText(context, "Su reserva fue registrada exitosamente", Toast.LENGTH_SHORT).show();
              }
          });

          alert.setNegativeButton("No", new DialogInterface.OnClickListener() {

              @Override
              public void onClick(DialogInterface dialog, int which) {
                  dialog.dismiss();
              }
          });

          alert.show();

      });
      /*  holder.imgview.setOnClickListener((v)->{
            Intent intent=new Intent(context, ListaProfeCurso.class);
            intent.putExtra("nombreprofe" , listProfeCurso.get(position).getNombres() + " " + listProfeCurso.get(position).getApellidos());
            intent.putExtra("idprofe" , listProfeCurso.get(position).getIdprofe());
            context.startActivity(intent);
        });
        */
  /*holder.imgview.setClickable(new View.OnClickListener() {

      @Override
      public void onClick(View view) {
          Intent intent = new Intent(MainActivity.this,ProfePerfil.class);
          startActivity(intent);
      }
  });*/
     /*   holder.cita_paciente_txt.setText(String.valueOf(listaCitasMedicas.get(position).getPaciente()));
        holder.cita_especialidad_txt.setText(String.valueOf(listaCitasMedicas.get(position).getEspecialidad()));
        holder.cita_clinica_txt.setText(String.valueOf(listaCitasMedicas.get(position).getClinica()));
 */
    }

    @Override
    public int getItemCount() {
        return listHorarios.size();
        //return 0;
    }

    public  class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView horario_diatexto_txxt, horario_dianro_txt, horario_hora_txt ;
         CardView cdview;
        //  ImageView imgview ;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            horario_diatexto_txxt = itemView.findViewById(R.id.horario_diatexto_txt);
            horario_dianro_txt = itemView.findViewById(R.id.horario_dianro_txt);
            horario_hora_txt = itemView.findViewById(R.id.horario_hora_txt);
            cdview= itemView.findViewById(R.id.cdvHorarioProfe);
            // profe_tarifa_txt=itemView.findViewById(R.id.profe_tarifa_txt);
         //   imgview= itemView.findViewById(R.id.profeImageView);

//                cdview.setOnClickListener();

     /*       cita_doctor_txt = itemView.findViewById(R.id.cita_doctor_txt);
            cita_paciente_txt = itemView.findViewById(R.id.cita_paciente_txt);
            cita_especialidad_txt = itemView.findViewById(R.id.cita_especialidad_txt);
            cita_clinica_txt = itemView.findViewById(R.id.cita_clinica_txt);
            cita_costo_txt = itemView.findViewById(R.id.cita_costo_txt);

            */

        }

        @Override
        public void onClick(View v) {
            final int position = (int) itemView.getTag();
            Toast.makeText(itemView.getContext(), Integer.toString(position), Toast.LENGTH_SHORT).show();
        }
    }

}
