package com.upcmoviles.appmobile;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.upcmoviles.appmobile.model.Curso;
import com.upcmoviles.appmobile.model.ProfeCurso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ListaProfeCurso extends AppCompatActivity {
    RecyclerView recyclerView;
    ArrayList<ProfeCurso> listaProfeCurso = new ArrayList<>();
    ProfeCursoAdapter adapter;
    //ListView lstProfecurso;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_profe_curso);
        
        //int idcurso=Integer.getInteger( getIntent().getExtras().getString("id_curso"));
        int idcurso= getIntent().getExtras().getInt("id_curso");
        String nombrecurso= getIntent().getExtras().getString("nombre_curso");
        getProfesbyCursos(idcurso,nombrecurso);
    }

    private void getProfesbyCursos(int idcurso, String nombrecurso)
    {
        String url = "http://atasilla.atwebpages.com/index.php/profes/" + idcurso ;

        System.out.println("Intentando conectar al servicio profe - idcurso :" + idcurso);
        StringRequest peticion = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    JSONArray arreglo = new JSONArray(response);
                    List<String> items = new ArrayList<>();
                    ProfeCurso profecurso= new ProfeCurso();
                    System.out.println("Datos econtrados " + arreglo.length());
                    for(int i=0;i<arreglo.length();i++){
                        JSONObject object = arreglo.getJSONObject(i);
                        profecurso=new ProfeCurso();
                        profecurso.setNombres(object.getString("nombres"));
                        profecurso.setApellidos(object.getString("apellido paterno"));
                        profecurso.setFileimagen(object.getString("fileimagen"));
                        profecurso.setTarifahora(object.getDouble("tarifahora"));
                        profecurso.setNrocelular(object.getString("nrocelular"));
                        profecurso.setIdprofe(object.getInt("idprofesor"));
                        profecurso.setIdcurso(idcurso);
                        profecurso.setNombrecurso(nombrecurso);

                        //   items.add(object.getString("nombre") + " (S/." + object.getString("precio") +")");
                        listaProfeCurso.add(profecurso);
                        // ArrayAdapter<Curso> adapter = new ArrayAdapter<Curso>(ListaCursos.this, android.R.layout.simple_list_item_1,items);
                        // lstProductos.setAdapter(adapter);
                        System.out.println("nombre curso:" + object.getString("nombres"));

                    }

                    recyclerView = findViewById(R.id.recyclerView);
                    System.out.println("Leyendo servicio");
                    adapter = new ProfeCursoAdapter(ListaProfeCurso.this,listaProfeCurso);
                    recyclerView.setAdapter(adapter);
                    System.out.println( "nro filas: " + listaProfeCurso.size() );
                    recyclerView.setLayoutManager(new LinearLayoutManager(ListaProfeCurso.this));
                    System.out.println("Fin servicio");

                }catch (JSONException e){
                    Toast.makeText(ListaProfeCurso.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("error" + error.getMessage());
                Toast.makeText(ListaProfeCurso.this, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        RequestQueue cola = Volley.newRequestQueue(this);
        cola.add(peticion);

        recyclerView = findViewById(R.id.recyclerView);
        System.out.println("Leyendo servicio");
        adapter = new ProfeCursoAdapter(ListaProfeCurso.this,listaProfeCurso);
        recyclerView.setAdapter(adapter);
        System.out.println( "nro filas: " + listaProfeCurso.size() );
        recyclerView.setLayoutManager(new LinearLayoutManager(ListaProfeCurso.this));
        System.out.println("Fin servicio");
        //return  listaCursos;
    }



}